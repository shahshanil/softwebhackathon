﻿using System;
namespace PARKAR
{
	internal class ServiceHelper
	{
		public static T GetResponse<T>(string response)
		{
			T dataResult = Activator.CreateInstance<T>();
			string ExceptionMessage = string.Empty;

			try
			{
				dataResult = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(response);
			}
			catch (Exception ex)
			{
				ExceptionMessage = ex.Message;
				return dataResult;
			}
			return dataResult;
		}
	}
}
