﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PARKAR
{
	public class HttpBaseClient
	{
		protected readonly string Endpoint;
		HttpClient httpClient;
		public HttpBaseClient(string endpoint)
		{
			Endpoint = endpoint;
			httpClient = new HttpClient();
		}
		private void SetHader()
		{
			var contenttype = new MediaTypeWithQualityHeaderValue("application/json");
			httpClient.DefaultRequestHeaders.Accept.Add(contenttype);

		}
		public async Task<string> Get()
		{
			HttpResponseMessage responseMessage;
			string response;

			try
			{
				this.SetHader(); ;
				responseMessage = await httpClient.GetAsync(Endpoint);
				if (!responseMessage.IsSuccessStatusCode)
				{
					response = BuildErrorMessage(Constants.SERVER_CONNECTION_ERROR);
					return response;
				}
				response = await responseMessage.Content.ReadAsStringAsync();
			}
			catch (HttpRequestException exception)
			{
				response = BuildErrorMessage(exception.Message);
			}
			catch (Exception exception)
			{
				response = BuildErrorMessage(exception.Message);
			}
			finally
			{
				httpClient.Dispose();

			}
			return response;
		}

		public async Task<string> Put(string payload)
		{
			var httpClient = new HttpClient();
			HttpResponseMessage responseMessage;
			string result = null;

			try
			{
				;
				this.SetHader();
				responseMessage = await httpClient.PutAsync(Endpoint, new StringContent(payload, System.Text.Encoding.UTF8, "application/json"));
				if (!responseMessage.IsSuccessStatusCode)
				{
					result = BuildErrorMessage(Constants.SERVER_CONNECTION_ERROR);
					return result;
				}

				result = await responseMessage.Content.ReadAsStringAsync();
			}
			catch (HttpRequestException exception)
			{
				result = BuildErrorMessage(exception.Message);
			}
			catch (Exception exception)
			{
				result = BuildErrorMessage(exception.Message);
			}
			finally
			{
				httpClient.Dispose();
			}

			return result;
		}

		public async Task<string> Post(string payload)
		{
			var httpClient = new HttpClient();
			HttpResponseMessage responseMessage;
			string result = null;

			try
			{
				this.SetHader(); ;
				responseMessage = await httpClient.PostAsync(Endpoint, new StringContent(payload, System.Text.Encoding.UTF8, "application/json"));
				if (!responseMessage.IsSuccessStatusCode)
				{
					result = BuildErrorMessage(Constants.SERVER_CONNECTION_ERROR);
					return result;
				}

				result = await responseMessage.Content.ReadAsStringAsync();

			}
			catch (HttpRequestException exc)
			{
				result = BuildErrorMessage(exc.Message);
			}
			catch (Exception exc)
			{
				result = BuildErrorMessage(exc.Message);
			}
			finally
			{
				httpClient.Dispose();
			}

			return result;
		}

		public string BuildErrorMessage(string error)
		{
			ResponseMessage msg;
			if (error == null)
			{
				msg = new ResponseMessage()
				{
					Message = Constants.SERVER_CONNECTION_ERROR,
					Status = false
				};
			}
			else {
				msg = new ResponseMessage()
				{
					Message = error,
					Status = false
				};
			}
			return Newtonsoft.Json.JsonConvert.SerializeObject(msg);
		}
	}

}
