﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Apis.Services;
using Google.Apis.Vision.v1;
using Google.Apis.Vision.v1.Data;
using Xamarin.Forms;

namespace PARKAR
{
	public partial class InOutPage : ContentPage
	{
		public InOutPage()
		{
			InitializeComponent();
			InClick.Clicked += async (sender, e) =>
			  {
				  await this.Navigation.PushAsync(new PARKARPage());
			  };
			OutClick.Clicked += (sender, e) =>
			 {
				 this.Navigation.PushAsync(new PARKARPage());
			 };
		}

		public string gblImgPath;


		public async Task<VisionService> CreateAuthorizedClient()
		{
			GoogleCredential credential =
				await GoogleCredential.GetApplicationDefaultAsync();
			// Inject the Cloud Vision scopes
			if (credential.IsCreateScopedRequired)
			{
				credential = credential.CreateScoped(new[]
				{
					VisionService.Scope.CloudPlatform
				});
			}
			return new VisionService(new BaseClientService.Initializer
			{
				HttpClientInitializer = credential
			});
		}

		public IList<AnnotateImageResponse> DetectText(VisionService vision, string imagePath)
		{

			// Convert image to Base64 encoded for JSON ASCII text based request   
			byte[] imageArray = System.IO.File.ReadAllBytes(imagePath);
			string imageContent = Convert.ToBase64String(imageArray);
			gblImgPath = imageContent;
			// Post text detection request to the Vision API
			var responses = vision.Images.Annotate(
				new BatchAnnotateImagesRequest()
				{
					Requests = new[] {
					new AnnotateImageRequest() {
						Features = new [] { new Feature() { Type =
						  "TEXT_DETECTION"}},
						Image = new Google.Apis.Vision.v1.Data.Image() { Content = imageContent }
					}
			   }
				}).Execute();
			return responses.Responses;
		}

		async void Handle_Clicked(object sender, System.EventArgs e)
		{
			//var photoResult = await DependencyService.Get<INativeHelpers> ().TakePictureAsync ();

			var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
			{

				Directory = "Sample",
				Name = DateTime.Now.ToString("yyyyMMddhhmmssffff") + ".jpg"
			});

			if (file == null)
				return;

			ShowImage(file.Path);
		}

		public async void ShowImage(string imagePath)
		{
			HackTestPage sample = new HackTestPage();

			VisionService vision = await sample.CreateAuthorizedClient();




			// Use the client to get text annotations for the given image
			IList<AnnotateImageResponse> result = sample.DetectText(vision, imagePath);
			// Check for valid text annotations in response
			if (result[0].TextAnnotations != null)
			{
				// Loop through and output text annotations for the image
				foreach (var response in result)
				{
					Console.WriteLine("Text found in image: " + imagePath);
					Console.WriteLine();
					foreach (var text in response.TextAnnotations)
					{
						Console.WriteLine(text.Description);
						Console.Write("(Bounding Polygon: ");
						var index = 0;
						foreach (var point in text.BoundingPoly.Vertices)
						{
							if (index > 0) Console.Write(", ");
							Console.Write("[" + point.X + "," + point.Y + "]");
							index++;
						}
						Console.Write(")");
						Console.WriteLine(Environment.NewLine);
					}
				}
			}
			else {
				if (result[0].Error == null)
				{
					Console.WriteLine("No text found.");
				}
				else {
					Console.WriteLine("Not a valid image.");
				}
			}
		}
	}
}
