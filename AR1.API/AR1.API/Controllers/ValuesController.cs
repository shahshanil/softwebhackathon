﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AR.API.Models;
using AR1.API;
using Google.Apis.Vision.v1;
using Google.Apis.Vision.v1.Data;
using AR1.API.Models;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace AR.API.Controllers
{
    public class ValuesController : ApiController
    {
        //GET api/values/CheckOut
        [HttpGet]
        public HttpResponseMessage CheckOut(string id)
        {
            id = Regex.Replace(id, @"[^\w\d]", "");
            ARDBEntities context = new ARDBEntities();
            ResponseInfo response = new ResponseInfo();
            try
            {
                var _parkerDetail = context.ParkarDetails.Where(x => x.VehicleNumber.Contains(id)).FirstOrDefault();
                if (_parkerDetail == null)
                    throw new Exception("Invalid Vehical Number");

                _parkerDetail.CheckOutDate = DateTime.Now;
                context.SaveChanges();

                response.Status = true;
                response.Message = "Check out time updated successfully";
                response.Data = _parkerDetail;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                response.Data = null;
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
        }

        // POST api/values/CheckIn
        [HttpPost]
        public HttpResponseMessage CheckIn([FromBody]ParkarDetail value)
        {
            ARDBEntities context = new ARDBEntities();
            ResponseInfo response = new ResponseInfo();
            try
            {
                var model = context.ParkarDetails.Where(x => x.Id == value.Id).FirstOrDefault();
                if (model != null)
                {
                    model.FullName = value.FullName;
                    model.PhoneNumber = value.PhoneNumber;
                    

                    model.VehicleNumber = Regex.Replace(value.VehicleNumber, @"[^\w\d]", "");
                    model.CheckInDate = DateTime.Now;

                    context.SaveChanges();

                    response.Status = true;
                    response.Message = "Check in time inserted successfully";
                    response.Data = null;
                }
                else
                {
                    response.Status = false;
                    response.Message = "Invalid Id";
                    response.Data = null;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                response.Data = null;
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
        }

        [HttpPost]
        public HttpResponseMessage PostVehicleImage([FromBody]ImageDetails model)
        {
            ARDBEntities context = new ARDBEntities();
            ResponseInfo responseInfo = new ResponseInfo();
            String vehicalNumber = string.Empty;
            try
            {
                var httpRequest = HttpContext.Current.Request;

                byte[] bytes = Convert.FromBase64String(model.img);


                if (bytes != null && bytes.Length > 0)
                {

                    int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB
                    MaxContentLength *= 2; //Give your size.

                    if (bytes.Length > MaxContentLength)
                    {
                        responseInfo.Status = false;
                        responseInfo.Message = "Please Upload a file upto 2 mb.";
                        responseInfo.Data = string.Empty;
                        return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        //var filePath = HttpContext.Current.Server.MapPath("~/VehicleImage/" + postedFile.FileName + extension);
                        //postedFile.SaveAs(filePath);

                        var filePath = HttpContext.Current.Server.MapPath("~/VehicleImage/" + Guid.NewGuid() + ".jpg");
                        File.WriteAllBytes(filePath, bytes);
                        System.IO.FileInfo postedFile = new System.IO.FileInfo(filePath);

                        #region Save into DB

                        FileUploadDBModel fileUploadModel = new FileUploadDBModel();

                        fileUploadModel.FileName = postedFile.Name;
                        fileUploadModel.FilePath = filePath;
                        fileUploadModel.File = bytes;
                        VisionService vision = AR.GoogleCloud.TextDetectionSample.CreateAuthorizedClient();
                        // Use the client to get text annotations for the given image
                        IList<AnnotateImageResponse> result = AR.GoogleCloud.TextDetectionSample.DetectText(
                            vision, fileUploadModel.FilePath);
                        // Check for valid text annotations in response
                        if (result.Count > 0 && result[0].TextAnnotations.Count() > 0)
                        {
                            var textresult = result[0];
                            vehicalNumber = textresult.TextAnnotations[0].Description;

                            //fileUploadModel.file = uploadfile;
                            var message1 = string.Format("Vehicle Image Updated Successfully.");
                            responseInfo.Status = true;
                            responseInfo.Message = message1;
                            context.FileUploadDBModels.Add(fileUploadModel);

                            ParkarDetail _parkerDetail = new ParkarDetail();
                            _parkerDetail.FullName = string.Empty;
                            _parkerDetail.PhoneNumber = string.Empty;
                            _parkerDetail.VehicleNumber = Regex.Replace(vehicalNumber, @"[^\w\d]", "");

                            context.ParkarDetails.Add(_parkerDetail);
                            context.SaveChanges();

                            FileResponse response = new FileResponse();
                            response.id = _parkerDetail.Id;
                            response.value = vehicalNumber;

                            responseInfo.Data = response;

                            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
                        }
                        else
                        {
                            var message = string.Format("enable to get data from image.");
                            responseInfo.Status = false;
                            responseInfo.Message = message;
                            responseInfo.Data = "";
                            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
                        }
                        #endregion Save into DB
                    }

                }
                responseInfo.Status = false;
                responseInfo.Message = "Please Upload a vehicle image.";
                return Request.CreateResponse(HttpStatusCode.OK, vehicalNumber, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                responseInfo.Status = false;
                responseInfo.Message = ex.Message;
                responseInfo.Data = vehicalNumber;
            }
            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
        }

        [HttpPost]
        public HttpResponseMessage CheckoutVehicleImage([FromBody]ImageDetails model)
        {
            
            ResponseInfo responseInfo = new ResponseInfo();
            String vehicalNumber = string.Empty;
            try
            {
                var httpRequest = HttpContext.Current.Request;

                byte[] bytes = Convert.FromBase64String(model.img);


                if (bytes != null && bytes.Length > 0)
                {

                    int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB
                    MaxContentLength *= 2; //Give your size.

                    if (bytes.Length > MaxContentLength)
                    {
                        responseInfo.Status = false;
                        responseInfo.Message = "Please Upload a file upto 2 mb.";
                        responseInfo.Data = string.Empty;
                        return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        //var filePath = HttpContext.Current.Server.MapPath("~/VehicleImage/" + postedFile.FileName + extension);
                        //postedFile.SaveAs(filePath);

                        var filePath = HttpContext.Current.Server.MapPath("~/VehicleImage/" + Guid.NewGuid() + ".jpg");
                        File.WriteAllBytes(filePath, bytes);
                        System.IO.FileInfo postedFile = new System.IO.FileInfo(filePath);

                        #region Save into DB

                        FileUploadDBModel fileUploadModel = new FileUploadDBModel();

                        fileUploadModel.FileName = postedFile.Name;
                        fileUploadModel.FilePath = filePath;
                        fileUploadModel.File = bytes;
                        VisionService vision = AR.GoogleCloud.TextDetectionSample.CreateAuthorizedClient();
                        // Use the client to get text annotations for the given image
                        IList<AnnotateImageResponse> result = AR.GoogleCloud.TextDetectionSample.DetectText(
                            vision, fileUploadModel.FilePath);
                        // Check for valid text annotations in response
                        if (result.Count > 0 && result[0].TextAnnotations.Count() > 0)
                        {
                            var textresult = result[0];
                            vehicalNumber = textresult.TextAnnotations[0].Description;

                            //fileUploadModel.file = uploadfile;
                            var message1 = string.Format("Vehicle Image Updated Successfully.");
                            responseInfo.Status = true;
                            responseInfo.Message = message1;
                            
                            FileResponse response = new FileResponse();
                            response.id = 0;
                            response.value = Regex.Replace(vehicalNumber, @"[^\w\d]", ""); 
                            responseInfo.Data = response;

                            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
                        }
                        else
                        {
                            var message = string.Format("enable to get data from image.");
                            responseInfo.Status = false;
                            responseInfo.Message = message;
                            responseInfo.Data = "";
                            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
                        }
                        #endregion Save into DB
                    }

                }
                responseInfo.Status = false;
                responseInfo.Message = "Please Upload a vehicle image.";
                return Request.CreateResponse(HttpStatusCode.OK, vehicalNumber, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                responseInfo.Status = false;
                responseInfo.Message = ex.Message;
                responseInfo.Data = vehicalNumber;
            }
            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
        }

        [HttpPost]
        public HttpResponseMessage GetImageDetails([FromBody]ImageDetails model)
        {
            //ARDBEntities context = new ARDBEntities();
            ResponseInfo responseInfo = new ResponseInfo();
            String imageLabel = string.Empty;
            try
            {
              //  var httpRequest = HttpContext.Current.Request;

                byte[] bytes = Convert.FromBase64String(model.img);


                if (bytes != null && bytes.Length > 0)
                {

                    int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB
                    MaxContentLength *= 2; //Give your size.

                    if (bytes.Length > MaxContentLength)
                    {
                        responseInfo.Status = false;
                        responseInfo.Message = "Please Upload a file upto 2 mb.";
                        responseInfo.Data = string.Empty;
                        return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        //var filePath = HttpContext.Current.Server.MapPath("~/VehicleImage/" + postedFile.FileName + extension);
                        //postedFile.SaveAs(filePath);

                        var filePath = HttpContext.Current.Server.MapPath("~/VehicleImage/" + Guid.NewGuid() + ".jpg");
                        File.WriteAllBytes(filePath, bytes);
                        System.IO.FileInfo postedFile = new System.IO.FileInfo(filePath);

                        #region Save into DB
                        VisionService vision = AR.LabelDetection.LabelDetection.CreateAuthorizedClient();
                        // Use the client to get text annotations for the given image
                        IList<AnnotateImageResponse> result = AR.LabelDetection.LabelDetection.DetectLabels(
                            vision, filePath);
                        // Check for valid text annotations in response
                        if (result.Count > 0 && result[0].LabelAnnotations.Count() > 0)
                        {
                            var textresult = result[0];
                            foreach (var item in result[0].LabelAnnotations.OrderByDescending(x=>x.Score))
                            {
                                imageLabel = imageLabel + item.Description + ", ";
                            }

                            //context.SaveChanges();

                            FileResponse response = new FileResponse();
                            response.id = 0;
                            response.value = imageLabel;

                            responseInfo.Status = true;
                            responseInfo.Message = string.Empty;
                            responseInfo.Data = response;

                            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
                        }
                        else
                        {
                            var message = string.Format("enable to get data from image.");
                            responseInfo.Status = false;
                            responseInfo.Message = message;
                            responseInfo.Data = "";
                            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
                        }
                        #endregion Save into DB
                    }

                }
                responseInfo.Status = false;
                responseInfo.Message = "Please Upload a vehicle image.";
                return Request.CreateResponse(HttpStatusCode.OK, imageLabel, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                responseInfo.Status = false;
                responseInfo.Message = ex.Message;
                responseInfo.Data = imageLabel;
            }
            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, Configuration.Formatters.JsonFormatter);
        }
    }
}