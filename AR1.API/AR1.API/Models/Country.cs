﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AR.API.Models
{
    public class ResponseInfo
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }


    public class ParkerDetailModel
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public string VehicleNumber { get; set; }
        public Nullable<System.DateTime> CheckInDate { get; set; }
        public Nullable<System.DateTime> CheckOutDate { get; set; }
    }
}