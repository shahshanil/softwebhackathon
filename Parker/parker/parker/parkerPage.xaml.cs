﻿using BigTed;
using Xamarin.Forms;

namespace parker
{
	public partial class parkerPage : ContentPage
	{
		BaseResponse<InOutResponse> response;
		public parkerPage (BaseResponse<InOutResponse> response, string img, bool InOut)
		{
			this.response = response;
			InitializeComponent ();
			imgPlat.Source = img;
			PlatNumber.Text = response.Data.value;
			Submit.Clicked += async (sender, e) => {
				var client = new HttpRequestHelper ();
				if (!InOut) {
					PhoneNumber.IsVisible = false;
					if (response != null) {
						BTProgressHUD.Show ();
						response = await client.CreateGetResponse<BaseResponse<InOutResponse>> ("Checkout/" + PlatNumber.Text);
						BTProgressHUD.Dismiss ();
						//PhoneNumber.Text = response.Data.;
					}
				} else {

					var request = new CheckInRequest () { Id = response.Data.id, VehicleNumber = PlatNumber.Text, PhoneNumber = PhoneNumber.Text, FullName = "" };
					BTProgressHUD.Show ();
					response = await client.CreatePostRequest<BaseResponse<InOutResponse>> ("CheckIn", request);
					BTProgressHUD.Dismiss ();
				}
				if (response.Status) {
					await this.Navigation.PopAsync ();
				} else {
					await this.DisplayAlert ("PARKAR", response.Message, "Cancel");
				}
			};
			Cancel.Clicked += async (sender, e) => {
				await this.Navigation.PopAsync ();
			};
		}
	}
}
