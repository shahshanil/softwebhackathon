﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace parker
{
	public partial class ImageDetailView : ContentPage
	{
		public ImageDetailView (string displaystring, string imagePath)
		{
			InitializeComponent ();

			ImageContainer.Source = imagePath;
			ContentLabel.Text = displaystring;
		}
	}
}
