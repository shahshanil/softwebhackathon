﻿using System;
namespace parker
{
	public class ErrorMessage
	{
		public ErrorMessage ()
		{
		}

		public string Message { get; set; }

		public string StackTrace { get; set; }

	}
}
