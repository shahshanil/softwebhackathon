﻿using System;
namespace parker
{
	public class ResponseMessage
	{
		public bool Status { get; set; }
		public string Message { get; set; }
	}
}
