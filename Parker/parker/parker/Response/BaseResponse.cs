﻿using System;
using Newtonsoft.Json;

namespace parker
{
	public class BaseResponse<T>
	{
		//[JsonProperty ("Status")]
		public bool Status { get; set; }

		//[JsonProperty ("Message")]
		public string Message { get; set; }

		[JsonProperty ("Data")]
		public T Data { get; set; }
	}
}
