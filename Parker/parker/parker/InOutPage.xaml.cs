﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using BigTed;
using Plugin.Media;
using Xamarin.Forms;

namespace parker
{
	public partial class InOutPage : ContentPage
	{
		string img;
		string nos;
		public InOutPage ()
		{
			InitializeComponent ();
			//InClick.Clicked += async (sender, e) => {
			//	await this.Navigation.PushAsync (new parkerPage ());
			//};
			//OutClick.Clicked += (sender, e) => {
			//	this.Navigation.PushAsync (new parkerPage ());
			//};
		}

		public string gblImgPath;



		async void In_Clicked (object sender, EventArgs e)
		{
			await NewMethod ("PostVehicleImage");
		}
		public BaseResponse<InOutResponse> response;

		async Task NewMethod (string methodname)
		{
			var file = await CrossMedia.Current.TakePhotoAsync (new Plugin.Media.Abstractions.StoreCameraMediaOptions {

				Directory = "Sample",
				Name = DateTime.Now.ToString ("yyyyMMddhhmmssffff") + ".jpg"
			});

			if (file == null)
				return;


			ShowImage (file.Path);
			img = file.Path;

			var client = new HttpRequestHelper ();
			var request = new LoginRequest () { img = PathToString (file.Path) };
			BTProgressHUD.Show ();
			response = await client.CreatePostRequest<BaseResponse<InOutResponse>> (methodname, request);
			BTProgressHUD.Dismiss ();
			if (response != null && response.Status) {
				nos = response.Data != null ? response.Data.value : string.Empty;
				await this.Navigation.PushAsync (new parkerPage (response, img, true));
			}
		}

		async void Out_Clicked (object sender, EventArgs e)
		{
			var file = await CrossMedia.Current.TakePhotoAsync (new Plugin.Media.Abstractions.StoreCameraMediaOptions {

				Directory = "Sample",
				Name = DateTime.Now.ToString ("yyyyMMddhhmmssffff") + ".jpg"
			});

			if (file == null)
				return;


			ShowImage (file.Path);
			img = file.Path;

			var client = new HttpRequestHelper ();
			BTProgressHUD.Show ();
			var request = new LoginRequest () { img = PathToString (file.Path) };
			response = await client.CreatePostRequest<BaseResponse<InOutResponse>> ("CheckoutVehicleImage", request);
			BTProgressHUD.Dismiss ();
			if (response != null && response.Status) {
				nos = response.Data != null ? response.Data.value : string.Empty;
				await this.Navigation.PushAsync (new parkerPage (response, img, false));
			} else {
				if (response != null && !response.Message.IsEmpty ())
					await this.DisplayAlert ("PARKAR", response.Message, "OK");
			}

		}

		async void GetImageDetailClick (object sender, EventArgs e)
		{


			var file = await CrossMedia.Current.TakePhotoAsync (new Plugin.Media.Abstractions.StoreCameraMediaOptions {

				Directory = "Sample",
				Name = DateTime.Now.ToString ("yyyyMMddhhmmssffff") + ".jpg"
			});



			if (file == null)
				return;

			BTProgressHUD.Show ();

			ShowImage (file.Path);
			img = file.Path;


			//await this.Navigation.PushAsync (new ImageDetailView ("ifg ifh wiufh iwufh iufh qweiuh wiufh wiuefh wiufh wiufh wiufh wiufh weiu", file.Path));

			//			return;

			var client = new HttpRequestHelper ();

			var request = new ImageDetailRequest () { img = PathToString (file.Path) };
			var imageAttributesResponse = await client.CreatePostRequest<BaseResponse<ImageDetailResponse>> ("GetImageDetails", request);
			BTProgressHUD.Dismiss ();
			if (imageAttributesResponse.Status && imageAttributesResponse.Data != null) {
				nos = imageAttributesResponse.Data.value;
				await this.Navigation.PushAsync (new ImageDetailView (imageAttributesResponse.Data.value, file.Path));
			} else {
				if (response != null && !response.Message.IsEmpty ())
					await this.DisplayAlert ("PARKAR", response.Message, "OK");
			}
		}


		public async void ShowImage (string imagePath)
		{

		}

		public static string PathToString (string path)
		{
			string _base64String = string.Empty;
			if (!path.IsEmpty ()) {
				FileStream fs = new FileStream (path, FileMode.Open, FileAccess.Read);
				// Create a byte array of file stream length
				byte [] ImageData = new byte [fs.Length];
				//Read block of bytes from stream into the byte array
				fs.Read (ImageData, 0, Convert.ToInt32 (fs.Length));
				//Close the File Stream
				fs.Close ();
				return Convert.ToBase64String (ImageData);
			}
			return _base64String;

			// provide read access to the file
		}
	}
}