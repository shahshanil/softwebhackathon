﻿using System;
namespace parker
{
	public static class Constants
	{
		public const string SERVER_CONNECTION_ERROR = "Service is temporary unavailable. Please try later";
		public const string BASE_URL = "http://192.168.5.16:8080/api/values/";

		public static bool IsEmpty (this string str)
		{
			return String.IsNullOrEmpty (str) || String.IsNullOrWhiteSpace (str);
		}
	}
	public enum RepositoryType
	{
		Mock = 0,
		Real = 1
	}


}
