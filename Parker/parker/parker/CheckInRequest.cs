﻿namespace parker
{
	public class CheckInRequest
	{
		public int Id { get; set; }
		public string PhoneNumber { get; set; }
		public string VehicleNumber { get; set; }
		public string FullName { get; set; }

	}
}