﻿using System;
namespace parker
{
	public class Session
	{
		private static Session _Instance;

		/// <summary>
		/// Singleton 
		/// </summary>
		public static Session Instance {
			get {
				if (_Instance == null) {
					_Instance = new Session ();
				}
				return _Instance;
			}
		}
	}
}
