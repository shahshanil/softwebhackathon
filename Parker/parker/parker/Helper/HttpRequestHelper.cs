﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace parker
{
	public class HttpRequestHelper
	{
		protected readonly string ServiceEndpoint;

		public HttpRequestHelper ()
		{
			ServiceEndpoint = ServiceHelper.ServiceUrl;
		}

		public async Task<T> CreateGetWeatherResponse<T> (string ServiceAPIResourcePath)
		{
			try {
				var httpBaseClient = new HttpBaseClient (ServiceAPIResourcePath);

				var responseCRServiceContent = await httpBaseClient.Get ();
				return ServiceHelper.GetResponse<T> (responseCRServiceContent);
			} catch (Exception ex) {
				Debug.WriteLine (ex);
				throw;
			}
		}


		public async Task<T> CreateGetResponse<T> (string ServiceAPIResourcePath)
		{
			try {
				var httpBaseClient = new HttpBaseClient (System.IO.Path.Combine (new string [] {
					ServiceEndpoint,
					ServiceAPIResourcePath
				}));

				var responseCRServiceContent = await httpBaseClient.Get ();
				return ServiceHelper.GetResponse<T> (responseCRServiceContent);
			} catch (Exception ex) {
				Debug.WriteLine (ex);
				throw;
			}
		}

		public async Task<string> CreateImagePostRequest<T> (string ServiceAPIResourcePath, object payload)
		{
			var httpBaseClient = new HttpBaseClient (System.IO.Path.Combine (new string [] {
				ServiceEndpoint,
				ServiceAPIResourcePath
			}));
			var responseServiceContent = await httpBaseClient.Post (Newtonsoft.Json.JsonConvert.SerializeObject (payload));
			return Newtonsoft.Json.JsonConvert.DeserializeObject<string> (responseServiceContent);
		}

		public async Task<T> CreatePostRequest<T> (string ServiceAPIResourcePath, object payload)
		{
			var httpBaseClient = new HttpBaseClient (System.IO.Path.Combine (new string [] {
				ServiceEndpoint,
				ServiceAPIResourcePath
			}));
			if (payload.GetType ().ToString () != "System.String") {
				payload = Newtonsoft.Json.JsonConvert.SerializeObject (payload);
			}
			var responseServiceContent = await httpBaseClient.Post (payload.ToString ());
			return ServiceHelper.GetResponse<T> (responseServiceContent);
		}

		public async Task<T> CreateUploadFileRequest<T> (string ServiceAPIResourcePath, object payload)
		{
			string responseCRServiceContent = "";
			try {
				var httpBaseClient = new HttpBaseClient (System.IO.Path.Combine (new string [] {
				ServiceEndpoint,
				ServiceAPIResourcePath
			}));

				//if (payload.GetType ().ToString () != "System.String") {
				//	payload = Newtonsoft.Json.JsonConvert.SerializeObject (payload);
				//}
				//var responseCRServiceContent = await httpBaseClient.UploadFile(Newtonsoft.Json.JsonConvert.SerializeObject(payload).ToString());
				//var responseCRServiceContent = await httpBaseClient.UploadImage(payload);
				//responseCRServiceContent = await httpBaseClient.UploadFile (payload);

				//return ServiceHelper.GetResponse<T> (responseCRServiceContent);
			} catch (Exception ex) {
				Debug.WriteLine (ex);
			}
			return ServiceHelper.GetResponse<T> (responseCRServiceContent);
		}
	}
}
