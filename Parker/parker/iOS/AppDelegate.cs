﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
//using Intents;
using UIKit;

namespace parker.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			//INPreferences.RequestSiriAuthorization ((INSiriAuthorizationStatus status) => {
			//	// Respond to returned status
			//	switch (status) {
			//	case INSiriAuthorizationStatus.Authorized:
			//		break;
			//	case INSiriAuthorizationStatus.Denied:
			//		break;
			//	case INSiriAuthorizationStatus.NotDetermined:
			//		break;
			//	case INSiriAuthorizationStatus.Restricted:
			//		break;
			//	}
			//});


			LoadApplication (new App ());

			return base.FinishedLaunching (app, options);
		}
	}
}
